package hkkptsui.ust.ihome.iweather;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FragmentMain extends Fragment {

    TextView cityField;
    TextView updatedField;
    TextView detailsField;
    TextView currentTemperatureField;
    TextView maxTemperature;
    TextView minTemperature;
    ImageView weatherIcon;
    TextView currentDayOfTheWeek;

    private  static final int timeInterval = 9; // 8-1:does not count current time, 3 hrs/interval, 3*8=24 hr

    ImageView [] forecastIcon = new ImageView[timeInterval]; // request 10 forecast weather, 1 for current temp,0-8
    TextView [] forecastTemp = new TextView[timeInterval];
    TextView [] forecastTime = new TextView[timeInterval];
    int [] bundleTime = new int[timeInterval];
    double [] bundleTemp = new double[timeInterval];

    TextView bottomText;
    Button buttonForecast;
    Button buttonGraph;

    Bundle bundle = new Bundle();

    RelativeLayout fragmentContainer;
    RelativeLayout componentContainer;

    ProgressBar progressBar;

    // ????????????????
    Handler handler;

    public FragmentMain(){
        handler = new Handler();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        cityField = (TextView)rootView.findViewById(R.id.city_field);
        updatedField = (TextView)rootView.findViewById(R.id.updated_field);
        detailsField = (TextView)rootView.findViewById(R.id.details_field);
        currentTemperatureField = (TextView)rootView.findViewById(R.id.current_temperature_field);
        maxTemperature = (TextView)rootView.findViewById(R.id.maxTemperature);
        minTemperature = (TextView)rootView.findViewById(R.id.minTemperature);
        weatherIcon = (ImageView)rootView.findViewById(R.id.weather_icon);
        currentDayOfTheWeek = (TextView) rootView.findViewById(R.id.dayOfTheWeek);

        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        componentContainer = (RelativeLayout) rootView.findViewById(R.id.component);

        Resources resources = getResources();
        String nameOfViewInLayout;
        int id;
        for(int i = 0; i < timeInterval; ++i){
            nameOfViewInLayout = "forecastTemp" + i;
            id = resources.getIdentifier(nameOfViewInLayout, "id", getActivity().getPackageName());
            forecastTemp[i] = (TextView) rootView.findViewById(id);

            nameOfViewInLayout = "forecastIcon" + i;
            id = resources.getIdentifier(nameOfViewInLayout, "id", getActivity().getPackageName());
            forecastIcon[i] = (ImageView) rootView.findViewById(id);

            nameOfViewInLayout = "forecastTime" + i;
            id = resources.getIdentifier(nameOfViewInLayout, "id", getActivity().getPackageName());
            forecastTime[i] = (TextView) rootView.findViewById(id);
        }

        bottomText = (TextView) rootView.findViewById(R.id.bottomText);

        fragmentContainer = (RelativeLayout) rootView.findViewById(R.id.fragment_main_id);

        /*
        // Setting refreshButton
        refreshButton = (ImageButton) rootView.findViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
                startActivity(getActivity().getIntent());
            }
        });
        */
        /*
        buttonForecast = (Button) rootView.findViewById(R.id.buttonForecast);
        buttonForecast.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v2){
                Intent intent = new Intent(getActivity(), WeatherForecastActivity.class);
                startActivity(intent);
            }
        });
        */
        /*
        buttonGraph = (Button) rootView.findViewById(R.id.buttonGraph);
        buttonGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v3) {
                Intent intent = new Intent(getActivity(), test.class);
				/*
				for (int i = 0; i < timeInterval; ++i) {
					bundle.putInt("time" + i, bundleTime[i]);
					bundle.putDouble("temp" + i, bundleTemp[i]);
				}
				*//*
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        */
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        updateWeatherData(new CityPreference(getActivity()).getCity());
    }

    private void updateWeatherData(final String city){
        new Thread(){
            public void run(){
                final JSONObject json = RemoteFetch.getJSON(getActivity(), city, RemoteFetch.OPEN_WEATHER_MAP_API_3hrs_forecast);
                if(json == null){
                    handler.post(new Runnable(){
                        public void run(){
                            Toast.makeText(getActivity(),
                                    getActivity().getString(R.string.place_not_found),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    handler.post(new Runnable(){
                        public void run(){
                            renderWeather(json);
                        }
                    });
                }
            }
        }.start();
    }

    private void renderWeather(JSONObject json){
        try {
            if(json.getJSONObject("city").getString("name").equals("")){
                cityField.setText(json.getJSONObject("city").getString("country"));
            }
            else{
                cityField.setText(json.getJSONObject("city").getString("name").toUpperCase(Locale.US) +
                        ", " +
                        json.getJSONObject("city").getString("country"));
            }

            JSONArray list = json.getJSONArray("list");
            JSONObject currentDay = list.getJSONObject(2); // 0 - 7 (forecast 8 intervals, skip first 2 intervals)
            JSONObject weather = currentDay.getJSONArray("weather").getJSONObject(0);
            JSONObject main = currentDay.getJSONObject("main");

            // Setting current Date and time interval
            //DateFormat df = DateFormat.getDateTimeInstance();
            //String updatedOn = df.format(new Date(currentDay.getLong("dt")*1000));
            String updatedTime = currentDay.getString("dt_txt").substring(0, 16);
            updatedField.setText("Last update: " + updatedTime);

            SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.US);
            String day = sdf.format(new Date());
            currentDayOfTheWeek.setText(day);


            // Setting background image
            String description = weather.getString("description").toUpperCase(Locale.US);
            if(description.contains("RAIN"))
                fragmentContainer.setBackgroundResource(R.drawable.rainy);
            else if(description.contains("CLEAR"))
                fragmentContainer.setBackgroundResource(R.drawable.clear2);
            else if(description.contains("SNOW"))
                fragmentContainer.setBackgroundResource(R.drawable.snow);
            else if(description.contains("THUND"))
                fragmentContainer.setBackgroundResource(R.drawable.thunderstorm);
            else
                fragmentContainer.setBackgroundResource(R.drawable.clouds2);


            // Setting text content
            detailsField.setText(
                    description +
                            "\nHumidity: " + main.getString("humidity") + "%" +
                            "\nWind: " + currentDay.getJSONObject("wind").getDouble("speed") + " m/s");

            currentTemperatureField.setText(
                    String.format("%.1f", main.getDouble("temp"))+ "℃");
            maxTemperature.setText(
                    "Max " + String.format("%.1f", main.getDouble("temp_max")) + "℃");
            minTemperature.setText(
                    "Min " + String.format("%.1f", main.getDouble("temp_min"))+ "℃");


            // Setting main weather icon
            String iconID = weather.getString("icon");
            String nameOfView = "icon" + iconID;
            Resources resources = getResources();
            int id = resources.getIdentifier(nameOfView, "drawable", getActivity().getPackageName());
            weatherIcon.setImageResource(id);


            // Setting temperature and icon of forecast weather
            // skip first 3 intervals
            for(int i = 0; i < timeInterval ; ++i){
                // Setting time intervals
                currentDay = list.getJSONObject(i+3);
                weather = currentDay.getJSONArray("weather").getJSONObject(0);

                //Date date = new Date(currentDay.getLong("dt") * 1000);
                //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE", Locale.US);
                //String stringDay = simpleDateFormat.format(date);
                String currentTime = currentDay.getString("dt_txt");
                forecastTime[i].setText(currentTime.substring(11, 16));
                if(currentTime.substring(11, 12).equals("0"))
                    bundleTime[i] = Integer.parseInt(currentTime.substring(12, 13));
                else
                    bundleTime[i] = Integer.parseInt(currentTime.substring(11, 13));
                bundle.putInt("time" + i, bundleTime[i]);

                // Setting temperature text
                bundleTemp[i] = currentDay.getJSONObject("main").getDouble("temp");
                bundle.putDouble("temp" + i, bundleTemp[i]);
                forecastTemp[i].setText(String.format("%.1f", currentDay.getJSONObject("main").getDouble("temp")) + "℃");

                // Setting icon
                iconID = weather.getString("icon");
                nameOfView = "icon" + iconID;
                id = resources.getIdentifier(nameOfView, "drawable", getActivity().getPackageName());
                forecastIcon[i].setImageResource(id);
            }


            // Setting bottom text
            String rainVolume = null;
            if(currentDay.has("rain"))
                if(currentDay.getJSONObject("rain").has("3h"))
                    rainVolume = currentDay.getJSONObject("rain").getString("3h");
            if(rainVolume == null || rainVolume.length() == 0)
                rainVolume = "0";

            bottomText.setText(
                    "Max Temperature: " + main.getString("temp_max") + "℃" +
                            "\nMin Temperature: " + main.getString("temp_min") + "℃" +
                            "\nWind Speed: " + currentDay.getJSONObject("wind").getDouble("speed") + " m/s" +
                            "\nCloudiness: " + currentDay.getJSONObject("clouds").getDouble("all") + "%" +
                            "\nPressure: " + main.getString("pressure") + " hPa" +
                            "\nRain: " + rainVolume + " mm" +
                            "\n");

            progressBar.setVisibility(View.GONE);
            componentContainer.setVisibility(View.VISIBLE);

            ((MainActivity) getActivity()).dismissProgressbar();
        }catch(Exception e){
            Log.e("iWeather", "One or more fields not found in the JSON data");
        }
    }

    public void changeCity(String city){
        updateWeatherData(city);
    }

}
