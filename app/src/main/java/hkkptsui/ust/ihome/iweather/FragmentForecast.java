package hkkptsui.ust.ihome.iweather;


import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FragmentForecast extends Fragment {

    public static final int forecastingDays = 9;

    ImageView [] icon = new ImageView[forecastingDays]; // request 10 forecast weather, 1 for current temp,0-8
    TextView [] date = new TextView[forecastingDays];
    TextView [] dayOfTheWeek = new TextView[forecastingDays];
    TextView [] maxTemp = new TextView[forecastingDays];
    TextView [] minTemp = new TextView[forecastingDays];
    TextView [] description = new TextView[forecastingDays];
    TextView [] windSpeed = new TextView[forecastingDays];
    TextView[] clouds = new TextView[forecastingDays];

    ProgressBar progressBar;
    RelativeLayout componentContainer;

    Handler handler;

    public FragmentForecast(){
        handler = new Handler();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_forecast, container, false);

        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        componentContainer = (RelativeLayout) rootView.findViewById(R.id.Container);

        Resources resources = getResources();
        String nameOfViewInLayout;
        int id;
        for(int i = 0; i < forecastingDays; ++i){
            nameOfViewInLayout = "icon" + i;
            id = resources.getIdentifier(nameOfViewInLayout, "id", getActivity().getPackageName());
            icon[i] = (ImageView) rootView.findViewById(id);

            nameOfViewInLayout = "date" + i;
            id = resources.getIdentifier(nameOfViewInLayout, "id", getActivity().getPackageName());
            date[i] = (TextView) rootView.findViewById(id);

            nameOfViewInLayout = "day" + i;
            id = resources.getIdentifier(nameOfViewInLayout, "id", getActivity().getPackageName());
            dayOfTheWeek[i] = (TextView) rootView.findViewById(id);

            nameOfViewInLayout = "maxTemp" + i;
            id = resources.getIdentifier(nameOfViewInLayout, "id", getActivity().getPackageName());
            maxTemp[i] = (TextView) rootView.findViewById(id);

            nameOfViewInLayout = "minTemp" + i;
            id = resources.getIdentifier(nameOfViewInLayout, "id", getActivity().getPackageName());
            minTemp[i] = (TextView) rootView.findViewById(id);

            nameOfViewInLayout = "description" + i;
            id = resources.getIdentifier(nameOfViewInLayout, "id", getActivity().getPackageName());
            description[i] = (TextView) rootView.findViewById(id);

            nameOfViewInLayout = "windSpeed" + i;
            id = resources.getIdentifier(nameOfViewInLayout, "id", getActivity().getPackageName());
            windSpeed[i] = (TextView) rootView.findViewById(id);

            nameOfViewInLayout = "clouds" + i;
            id = resources.getIdentifier(nameOfViewInLayout, "id", getActivity().getPackageName());
            clouds[i] = (TextView) rootView.findViewById(id);
        }

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateWeatherData(new CityPreference(getActivity()).getCity());
    }

    private void updateWeatherData(final String city){
        new Thread(){
            public void run(){
                final JSONObject json = RemoteFetch.getJSON(getActivity(), city, RemoteFetch.OPEN_WEATHER_MAP_API_day_forecast);
                if(json == null){
                    handler.post(new Runnable(){
                        public void run(){
                            Toast.makeText(getActivity(),
                                    getActivity().getString(R.string.place_not_found),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    handler.post(new Runnable(){
                        public void run(){
                            renderWeather(json);
                        }
                    });
                }
            }
        }.start();
    }

    private void renderWeather(JSONObject json){
        try {

            JSONArray list = json.getJSONArray("list");

            // Setting temperature and icon of forecast weather
            for(int i = 0; i < forecastingDays ; ++i){
                JSONObject currentDay = list.getJSONObject(i); // 0 - 6 (forecast 7days)
                JSONObject weather = currentDay.getJSONArray("weather").getJSONObject(0); // "weather" is 1D array
                JSONObject temp = currentDay.getJSONObject("temp");

                // Setting main weather icon
                String iconID = weather.getString("icon");
                String nameOfView = "icon" + iconID;
                Resources resources = getResources();
                int id = resources.getIdentifier(nameOfView, "drawable", getActivity().getPackageName());
                icon[i].setImageResource(id);

                // Setting date
                Date dateInJSON = new Date(currentDay.getLong("dt") * 1000);
                SimpleDateFormat sdf = new SimpleDateFormat("d MMM", Locale.US);
                String day = sdf.format(dateInJSON);
                date[i].setText(day);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE", Locale.US);
                day = simpleDateFormat.format(dateInJSON);
                dayOfTheWeek[i].setText(day);

                char celsius = 0x00B0;
                maxTemp[i].setText(
                        "Max: " + String.format("%.1f", temp.getDouble("max")) + celsius +"C");
                minTemp[i].setText(
                        "Min: " + String.format("%.1f", temp.getDouble("min")) + celsius + "C");

                description[i].setText(weather.getString("description"));

                windSpeed[i].setText("Wind Speed: " + currentDay.getDouble("speed") + " m/s");
                clouds[i].setText("Clouds: " + currentDay.getDouble("clouds") + "%, Pressure: " + currentDay.getString("pressure") + " hPa");
            }

            progressBar.setVisibility(View.GONE);
            componentContainer.setVisibility(View.VISIBLE);
        }catch(Exception e){
            Log.e("oWeather", "One or more fields not found in the JSON data");
        }
    }

    public void changeCity(String city){
        updateWeatherData(city);
    }

}
